package util

import (
	"context"
	"fmt"

	"gitlab.com/mercury4/proto/proto_gen/common"
	"gitlab.com/mercury4/proto/proto_gen/mercury/service/question"
	"gitlab.com/mercury4/question/model"
)

func ConvertQuestions2PbMap(ctx context.Context, questions []*model.Question) map[int64]*question.Question {

	output := make(map[int64]*question.Question, len(questions))
	for _, v := range questions {
		q := ConvertQuestion2Pb(ctx, v)
		output[q.QuestionId] = q
	}
	return output
}

func ConvertQuestions2Pb(ctx context.Context, questions []*model.Question) []*question.Question {

	var output []*question.Question
	for _, v := range questions {
		q := ConvertQuestion2Pb(ctx, v)
		output = append(output, q)
	}
	return output
}

func ConvertQuestion2Pb(ctx context.Context, q *model.Question) *question.Question {

	output := &question.Question{}
	output.Title = q.Title
	output.Content = q.Content
	output.UserId = fmt.Sprintf("%d", q.UserId)
	return output
}

func ConvertAnswers2Pb(ctx context.Context, answers []*model.Answer) []*question.Answer {

	var output []*question.Answer
	for _, v := range answers {
		answer := ConvertAnswer2Pb(ctx, v)
		output = append(output, answer)
	}

	return output
}

func ConvertAnswers2PbMap(ctx context.Context, answers []*model.Answer) map[int64]*question.Answer {

	output := make(map[int64]*question.Answer, len(answers))
	for _, v := range answers {
		answer := ConvertAnswer2Pb(ctx, v)
		output[answer.AnswerId] = answer
	}

	return output
}

func ConvertAnswer2Pb(ctx context.Context, answer *model.Answer) *question.Answer {

	output := &question.Answer{
		AnswerId:   answer.AnswerId,
		Content:    answer.Content,
		UserId:     answer.UserId,
		QuestionId: answer.QuestionId,
	}

	return output
}

func ConvertPageData2Pb(ctx context.Context, pageData *model.PageData) *common.PageData {
	output := &common.PageData{}
	output.Total = pageData.Total
	output.HasNext = pageData.HasNext
	output.HasPrev = pageData.HasPrev

	return output
}

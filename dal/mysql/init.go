package mysql

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var (
	DB *gorm.DB
)

func Init(dns string) {
	db, err := gorm.Open("mysql", dns)
	if err != nil {
		panic(err)
	}

	DB = db
}

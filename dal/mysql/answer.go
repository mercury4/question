package mysql

import (
	"context"

	"gitlab.com/mercury4/question/model"
)

func CreateAnswer(ctx context.Context, answer *model.Answer) error {

	if err := DB.Create(answer).Error; err != nil {
		return err
	}
	return nil
}

func GetAnswersByQuestionId(ctx context.Context, questionId int64, offset, limit int64) (answers []*model.Answer, total int64, err error) {

	err = DB.Where("question_id=?", questionId).Offset(offset).Limit(limit).Find(&answers).Count(&total).Error
	if err != nil {
		return
	}
	return
}

func MGetAnswers(ctx context.Context, answerIds []int64) (answers []*model.Answer, err error) {

	err = DB.Where("answer_id in(?)", answerIds).Find(&answers).Error
	if err != nil {
		return
	}

	return
}

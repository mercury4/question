package mysql

import (
	"context"

	"gitlab.com/mercury4/question/model"
)

func CreateQuestion(ctx context.Context, question *model.Question) error {

	if err := DB.Create(question).Error; err != nil {
		return err
	}
	return nil
}

func GetQuestionById(ctx context.Context, qid int64) (question *model.Question, err error) {

	question = &model.Question{}
	err = DB.Where("question_id=?", qid).First(question).Error
	if err != nil {
		return
	}
	return
}

func GetQuestionByUserId(ctx context.Context, userId int64, offset, limit int64) (questions []*model.Question, total int64, err error) {

	err = DB.Where("user_id=?", userId).Offset(offset).Limit(limit).Find(&questions).Count(&total).Error
	if err != nil {
		return
	}
	return
}

func MGetQuestion(ctx context.Context, questionIds []int64) (questions []*model.Question, err error) {

	err = DB.Where("question_id in(?)", questionIds).Find(&questions).Error
	if err != nil {
		return
	}

	return
}

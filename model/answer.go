package model

import "time"

type Answer struct {
	Id         int64     `db:"id"`
	AnswerId   int64     `db:"answer_id"`
	Content    string    `db:"content"`
	QuestionId int64     `db:"question_id"`
	UserId     int64     `db:"user_id"`
	Status     int       `db:"status"`
	CreateTime time.Time `db:"create_time"`
	UpdateTime time.Time `db:"update_time"`
}

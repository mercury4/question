package model

type PageParams struct {
	Offset int64
	Limit  int64
}

type PageData struct {
	HasNext bool
	HasPrev bool
	Total   int64
}

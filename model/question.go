package model

import "time"

type QuestionStatus int
type QuestionAttr int

const (
	QuestionStatusDelete QuestionStatus = 0
	QuestionStatusNormal QuestionStatus = 1
)

const (
	QuestionAttrUnknown       QuestionAttr = 0x0
	QuestionAttrForbidComment QuestionAttr = 0x01
	QuestionAttrForbidAnswer  QuestionAttr = 0x10
	QuestionAttrSelfView      QuestionAttr = 0x100
	QuestionAttrPublicView    QuestionAttr = 0x1000
)

type Question struct {
	Id         int64     `db:"id"`
	QuestionId int64     `db:"question_id"`
	Title      string    `db:"title"`
	Content    string    `db:"content"`
	UserId     int64     `db:"user_id"`
	Attribute  int       `db:"attribute"`
	Status     int       `db:"status"`
	CreateTime time.Time `db:"create_time"`
	UpdateTime time.Time `db:"update_time"`
}

func (q *Question) IsCanAnswer() bool {

	//问题被删除了
	if QuestionStatus(q.Status) == QuestionStatusDelete {
		return false
	}

	//问题禁止回答，或者自见状态，不允许回答
	if QuestionAttr(q.Attribute)&QuestionAttrSelfView == 0x1 ||
		QuestionAttr(q.Attribute)&QuestionAttrForbidAnswer == 0x1 {
		return false
	}
	return true
}

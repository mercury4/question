package logic

import (
	"context"

	"gitlab.com/mercury4/pkg/id_gen"
	"gitlab.com/mercury4/question/dal/mysql"
	"gitlab.com/mercury4/question/model"
)

type CreateQuestionLogic struct {
	question *model.Question
	ctx      context.Context
}

func NewCreateQuestionLogic(ctx context.Context, question *model.Question) *CreateQuestionLogic {
	return &CreateQuestionLogic{
		ctx:      ctx,
		question: question,
	}
}

func (c *CreateQuestionLogic) Run() error {

	//1. 生成question id
	id, err := id_gen.GetId()
	if err != nil {
		return err
	}

	c.question.QuestionId = int64(id)
	//2. 敏感词过滤 TODO
	//3. 个人发帖频率控制 TODO
	//4. 入库
	err = mysql.CreateQuestion(c.ctx, c.question)
	if err != nil {
		return err
	}

	//5. 发布MQ消息
	return nil
}

func (c *CreateQuestionLogic) GetQuestion() *model.Question {
	return c.question
}

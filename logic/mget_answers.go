package logic

import (
	"context"

	"gitlab.com/mercury4/question/dal/mysql"
	"gitlab.com/mercury4/question/model"
)

type MGetAnswerLogic struct {
	answersId []int64
	answers   []*model.Answer
	ctx       context.Context
}

func NewMGetAnswerLogic(ctx context.Context, answersId []int64) *MGetAnswerLogic {
	return &MGetAnswerLogic{
		ctx:       ctx,
		answersId: answersId,
	}
}

func (c *MGetAnswerLogic) Run() error {
	//1. 读取对应的answer
	var err error
	c.answers, err = mysql.MGetAnswers(c.ctx, c.answersId)
	if err != nil {
		return err
	}

	return nil
}

func (c *MGetAnswerLogic) GetAnswers() []*model.Answer {
	return c.answers
}

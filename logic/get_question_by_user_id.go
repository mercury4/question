package logic

import (
	"context"

	"gitlab.com/mercury4/question/dal/mysql"
	"gitlab.com/mercury4/question/model"
)

type GetQuestionByUserIdLogic struct {
	userId   int64
	params   *model.PageParams
	question []*model.Question
	pageData *model.PageData
	ctx      context.Context
}

func NewGetQuestionByUserIdLogic(ctx context.Context, userId int64,
	params *model.PageParams) *GetQuestionByUserIdLogic {
	return &GetQuestionByUserIdLogic{
		ctx:      ctx,
		userId:   userId,
		params:   params,
		pageData: &model.PageData{},
	}
}

func (c *GetQuestionByUserIdLogic) Run() error {
	//1. 读取对应的answer
	var err error
	c.question, c.pageData.Total, err = mysql.GetQuestionByUserId(c.ctx, c.userId, c.params.Offset, c.params.Limit)
	if err != nil {
		return err
	}

	return nil
}

func (c *GetQuestionByUserIdLogic) GetQuestions() []*model.Question {
	return c.question
}

func (c *GetQuestionByUserIdLogic) GetPageData() *model.PageData {
	return c.pageData
}

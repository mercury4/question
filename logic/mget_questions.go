package logic

import (
	"context"

	"gitlab.com/mercury4/question/dal/mysql"
	"gitlab.com/mercury4/question/model"
)

type MGetQuestionLogic struct {
	questionIds []int64
	questions   []*model.Question
	ctx         context.Context
}

func NewMGetQuestionLogic(ctx context.Context, questionIds []int64) *MGetQuestionLogic {
	return &MGetQuestionLogic{
		ctx:         ctx,
		questionIds: questionIds,
	}
}

func (c *MGetQuestionLogic) Run() error {
	//1. 读取对应的answer
	var err error
	c.questions, err = mysql.MGetQuestion(c.ctx, c.questionIds)
	if err != nil {
		return err
	}

	return nil
}

func (c *MGetQuestionLogic) GetQuestions() []*model.Question {
	return c.questions
}

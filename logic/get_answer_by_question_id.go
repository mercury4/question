package logic

import (
	"context"

	"gitlab.com/mercury4/question/dal/mysql"
	"gitlab.com/mercury4/question/model"
)

type GetAnswerByQuestionIdLogic struct {
	questionId int64
	params     *model.PageParams
	answers    []*model.Answer
	question   *model.Question
	pageData   *model.PageData
	ctx        context.Context
}

func NewGetAnswerByQuestionIdLogic(ctx context.Context, questionId int64,
	params *model.PageParams) *GetAnswerByQuestionIdLogic {
	return &GetAnswerByQuestionIdLogic{
		ctx:        ctx,
		questionId: questionId,
		params:     params,
		pageData:   &model.PageData{},
	}
}

func (c *GetAnswerByQuestionIdLogic) loadQuestion() error {

	//加载question数据
	question, err := mysql.GetQuestionById(c.ctx, c.questionId)
	if err != nil {
		return err
	}

	c.question = question
	return nil
}

func (c *GetAnswerByQuestionIdLogic) Run() error {

	//0. 加载question
	err := c.loadQuestion()
	if err != nil {
		return err
	}

	//1. 读取对应的answer
	c.answers, c.pageData.Total, err = mysql.GetAnswersByQuestionId(c.ctx, c.questionId, c.params.Offset, c.params.Limit)
	if err != nil {
		return err
	}

	return nil
}

func (c *GetAnswerByQuestionIdLogic) GetAnswers() []*model.Answer {
	return c.answers
}

func (c *GetAnswerByQuestionIdLogic) GetQuestion() *model.Question {
	return c.question
}

func (c *GetAnswerByQuestionIdLogic) GetPageData() *model.PageData {
	return c.pageData
}

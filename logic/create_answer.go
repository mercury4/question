package logic

import (
	"context"
	"fmt"

	"gitlab.com/mercury4/pkg/id_gen"
	"gitlab.com/mercury4/question/dal/mysql"
	"gitlab.com/mercury4/question/model"
)

type CreateAnswerLogic struct {
	answer   *model.Answer
	question *model.Question
	ctx      context.Context
}

func NewCreateAnswerLogic(ctx context.Context, answer *model.Answer) *CreateAnswerLogic {
	return &CreateAnswerLogic{
		ctx:    ctx,
		answer: answer,
	}
}

func (c *CreateAnswerLogic) checkQuestion() error {

	//加载question数据
	question, err := mysql.GetQuestionById(c.ctx, c.answer.QuestionId)
	if err != nil {
		return err
	}

	c.question = question
	if !c.question.IsCanAnswer() {
		err = fmt.Errorf("question:%v is not allow answer", c.question.QuestionId)
		return err
	}
	return nil
}

func (c *CreateAnswerLogic) Run() error {

	//0. check question id 是否合法
	err := c.checkQuestion()
	if err != nil {
		return err
	}

	//1. 生成Answer id
	id, err := id_gen.GetId()
	if err != nil {
		return err
	}

	c.answer.AnswerId = int64(id)
	//2. 敏感词过滤 TODO
	//3. 个人发帖频率控制 TODO
	err = c.allow(c.ctx)
	if err != nil {
		return err
	}
	//4. 入库
	err = mysql.CreateAnswer(c.ctx, c.answer)
	if err != nil {
		return err
	}

	//5. 发布MQ消息
	return nil
}

func (c *CreateAnswerLogic) allow(ctx context.Context) error {
	return nil
}

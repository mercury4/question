package controller

import (
	"context"
	"fmt"
	"time"

	"github.com/ibinarytree/koala/logs"
	"gitlab.com/mercury4/proto/proto_gen/mercury/service/question"
	"gitlab.com/mercury4/question/logic"
	"gitlab.com/mercury4/question/model"
)

type CreateAnswerController struct {
}

//检查请求参数，如果该函数返回错误，则Run函数不会执行
func (s *CreateAnswerController) CheckParams(ctx context.Context, r *question.CreateAnswerRequest) (err error) {

	if len(r.Content) == 0 || r.QuestionId == 0 || r.UserId == 0 {
		err = fmt.Errorf("invalid parameter")
		logs.Error(ctx, "invalid paramter, r:%#v", r)
		return
	}
	return
}

func (s *CreateAnswerController) createAnswerModel(ctc context.Context, r *question.CreateAnswerRequest) *model.Answer {

	return &model.Answer{
		Content:    r.Content,
		QuestionId: r.QuestionId,
		UserId:     r.UserId,
		CreateTime: time.Now(),
		UpdateTime: time.Now(),
	}

}

//SayHello函数的实现
func (s *CreateAnswerController) Run(ctx context.Context, r *question.CreateAnswerRequest) (
	resp *question.CreateAnswerResponse, err error) {

	defer func() {
		logs.AddField(ctx, "req", r)
		logs.AddField(ctx, "resp", resp)
	}()

	answer := s.createAnswerModel(ctx, r)
	handle := logic.NewCreateAnswerLogic(ctx, answer)
	err = handle.Run()
	if err != nil {
		logs.Error(ctx, "create answer failed, err:%v", err)
		return
	}

	return
}

package controller

import (
	"context"
	"fmt"

	"github.com/ibinarytree/koala/logs"
	"gitlab.com/mercury4/proto/proto_gen/mercury/service/question"
	"gitlab.com/mercury4/question/logic"
	"gitlab.com/mercury4/question/util"
)

type MGetAnswerController struct {
}

//检查请求参数，如果该函数返回错误，则Run函数不会执行
func (s *MGetAnswerController) CheckParams(ctx context.Context, r *question.MGetAnswerRequest) (err error) {

	if len(r.AnswerIds) == 0 {
		err = fmt.Errorf("invalid parameter")
		logs.Error(ctx, "invalid pamater:%#v", r)
		return
	}
	return
}

//SayHello函数的实现
func (s *MGetAnswerController) Run(ctx context.Context, r *question.MGetAnswerRequest) (
	resp *question.MGetAnswerResponse, err error) {

	handle := logic.NewMGetAnswerLogic(ctx, r.AnswerIds)
	err = handle.Run()
	if err != nil {
		logs.Error(ctx, "process NewMGetAnswerLogic failed, err:%v", err)
		return
	}

	resp = &question.MGetAnswerResponse{}
	resp.AnswerMap = util.ConvertAnswers2PbMap(ctx, handle.GetAnswers())
	return
}

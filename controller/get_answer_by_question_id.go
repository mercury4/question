package controller

import (
	"context"
	"fmt"

	"github.com/ibinarytree/koala/logs"
	"gitlab.com/mercury4/proto/proto_gen/mercury/service/question"
	"gitlab.com/mercury4/question/logic"
	"gitlab.com/mercury4/question/model"
	"gitlab.com/mercury4/question/util"
)

type GetAnswerByQuestionIdController struct {
}

//检查请求参数，如果该函数返回错误，则Run函数不会执行
func (s *GetAnswerByQuestionIdController) CheckParams(ctx context.Context,
	r *question.GetAnswerByQuestionIdRequest) (err error) {
	if r.QuestionId == 0 {
		err = fmt.Errorf("invalid parameter")
		logs.Error(ctx, "invalid paramteer, r:%#v", r)
		return
	}
	return
}

func (s *GetAnswerByQuestionIdController) createParams(ctx context.Context,
	r *question.GetAnswerByQuestionIdRequest) *model.PageParams {

	return &model.PageParams{
		Offset: r.GetPageParam().GetOffset(),
		Limit:  r.GetPageParam().GetLimit(),
	}
}

//SayHello函数的实现
func (s *GetAnswerByQuestionIdController) Run(ctx context.Context, r *question.GetAnswerByQuestionIdRequest) (
	resp *question.GetAnswerByQuestionIdResponse, err error) {

	params := s.createParams(ctx, r)
	handle := logic.NewGetAnswerByQuestionIdLogic(ctx, r.QuestionId, params)
	err = handle.Run()
	if err != nil {
		logs.Error(ctx, "get answer by question id failed, err:%v", err)
		return
	}

	resp = s.packer(ctx, handle.GetAnswers(), handle.GetQuestion(), handle.GetPageData())
	return
}

func (s *GetAnswerByQuestionIdController) packer(ctx context.Context, answers []*model.Answer,
	q *model.Question, pageData *model.PageData) *question.GetAnswerByQuestionIdResponse {

	resp := &question.GetAnswerByQuestionIdResponse{}
	resp.Question = util.ConvertQuestion2Pb(ctx, q)
	resp.Answers = util.ConvertAnswers2Pb(ctx, answers)
	resp.PageData = util.ConvertPageData2Pb(ctx, pageData)

	return resp
}

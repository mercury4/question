package controller

import (
	"context"
	"fmt"
	"time"

	"github.com/ibinarytree/koala/logs"
	"gitlab.com/mercury4/proto/proto_gen/mercury/service/question"
	"gitlab.com/mercury4/question/logic"
	"gitlab.com/mercury4/question/model"
)

type CreateQuestionController struct {
}

//检查请求参数，如果该函数返回错误，则Run函数不会执行
func (s *CreateQuestionController) CheckParams(ctx context.Context, r *question.CreateQuestionRequest) (err error) {

	if len(r.Content) == 0 || len(r.Title) == 0 || r.UserId == 0 {
		err = fmt.Errorf("invalid parameter")
		logs.Error(ctx, "invalid paramter, r:%#v", r)
		return
	}
	return
}

func (s *CreateQuestionController) createQuestionModel(r *question.CreateQuestionRequest) *model.Question {

	return &model.Question{
		Title:      r.GetTitle(),
		Content:    r.GetContent(),
		UserId:     r.GetUserId(),
		CreateTime: time.Now(),
		UpdateTime: time.Now(),
	}
}

//SayHello函数的实现
func (s *CreateQuestionController) Run(ctx context.Context, r *question.CreateQuestionRequest) (
	resp *question.CreateQuestionResponse, err error) {

	defer func() {
		logs.AddField(ctx, "req", r)
		logs.AddField(ctx, "resp", resp)
	}()

	questionModel := s.createQuestionModel(r)
	handle := logic.NewCreateQuestionLogic(ctx, questionModel)
	err = handle.Run()
	if err != nil {
		logs.Error(ctx, "create question failed, r:%#v", r)
		return
	}

	resp.QuestionId = handle.GetQuestion().QuestionId
	return
}

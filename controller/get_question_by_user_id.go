package controller

import (
	"context"
	"fmt"

	"github.com/ibinarytree/koala/logs"
	"gitlab.com/mercury4/proto/proto_gen/mercury/service/question"
	"gitlab.com/mercury4/question/logic"
	"gitlab.com/mercury4/question/model"
	"gitlab.com/mercury4/question/util"
)

type GetQuestionByUserIdController struct {
}

//检查请求参数，如果该函数返回错误，则Run函数不会执行
func (s *GetQuestionByUserIdController) CheckParams(ctx context.Context, r *question.GetQuestionByUserIdRequest) (err error) {

	if r.UserId == 0 {
		err = fmt.Errorf("invalid parameter, user_id:%v", r.UserId)
		logs.Error(ctx, "invalid user_id:%v", r.GetUserId())
		return
	}
	return
}

func (s *GetQuestionByUserIdController) createPageParams(ctx context.Context, r *question.GetQuestionByUserIdRequest) *model.PageParams {

	pageParams := &model.PageParams{
		Offset: r.GetPageParam().GetOffset(),
		Limit:  r.GetPageParam().GetLimit(),
	}
	return pageParams
}

//SayHello函数的实现
func (s *GetQuestionByUserIdController) Run(ctx context.Context, r *question.GetQuestionByUserIdRequest) (
	resp *question.GetQuestionByUserIdResponse, err error) {

	defer func() {
		logs.AddField(ctx, "req", r)
		logs.AddField(ctx, "rsp", resp)
	}()

	pageParams := s.createPageParams(ctx, r)
	handle := logic.NewGetQuestionByUserIdLogic(ctx, r.UserId, pageParams)
	err = handle.Run()
	if err != nil {
		logs.Error(ctx, "NewGetQuestionByUserIdLogic process failed, err:%v", r.UserId)
		return
	}

	resp = &question.GetQuestionByUserIdResponse{}
	resp.Question = util.ConvertQuestions2Pb(ctx, handle.GetQuestions())
	resp.PageData = util.ConvertPageData2Pb(ctx, handle.GetPageData())

	return
}

package controller

import (
	"context"
	"fmt"

	"gitlab.com/mercury4/proto/proto_gen/mercury/service/question"
	"gitlab.com/mercury4/question/logic"
	"gitlab.com/mercury4/question/util"
)

type MGetQuestionController struct {
}

//检查请求参数，如果该函数返回错误，则Run函数不会执行
func (s *MGetQuestionController) CheckParams(ctx context.Context, r *question.MGetQuestionRequest) (err error) {

	if len(r.QuestionIds) == 0 {
		err = fmt.Errorf("invalid question ids ")
		return
	}
	return
}

//SayHello函数的实现
func (s *MGetQuestionController) Run(ctx context.Context, r *question.MGetQuestionRequest) (
	resp *question.MGetQuestionResponse, err error) {

	handle := logic.NewMGetQuestionLogic(ctx, r.GetQuestionIds())
	err = handle.Run()
	if err != nil {
		return
	}

	resp = &question.MGetQuestionResponse{}
	resp.GetQuestionMap = util.ConvertQuestions2PbMap(ctx, handle.GetQuestions())
	return
}

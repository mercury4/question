module gitlab.com/mercury4/question

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/ibinarytree/koala v1.9.15
	github.com/jinzhu/gorm v1.9.12
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gitlab.com/mercury4/pkg v0.0.0-20200531001254-5c98ec4c6daa
	gitlab.com/mercury4/proto v0.0.0-20200620095740-5bc42493a094
	google.golang.org/grpc v1.24.0
)
